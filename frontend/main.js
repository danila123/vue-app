import Vue from "vue";
import Root from "./components/Root.vue";

new Vue({
    el:'#root',
    render: h=>h(Root)
});