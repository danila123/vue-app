from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView


class IndexView(TemplateView):
    template_name = '../../templates/index/index.html'

    def get_context_data(self, **kwargs):
        context = {}

        context['title'] = 'Main Page'

        return context
